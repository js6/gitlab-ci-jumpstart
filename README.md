# Gitlab CI jumpstart

## Introduction

The aim of this package is to provide a jumpstart when using Gitlab CI for PHP projects 

After installing this package (see [setup](#setup)) you'll have the following things set up:

- A git pre-commit hook that will take the files in the commit, and put them through a syntax checker, followed by a formatting checker. It will also attempt to autocorrect the formatting issues where possible. Files that fail either the syntax opr the formatting check won't be able to be committed. 

- A base Gitlab CI config file that will check for syntax & formatting, run your unit tests and run a security check on your dependencies.

## Setup

> These setup steps assume you have enabled Gitlab CI for your project and have a docker enabled runner set up. 

There's only 2 steps required to start using Gitlab CI in your current project

- require this package =>  `composer require ...`
- run the setup script => `./vendor/bin/ci-setup`

The setup script will do 2 things 

1) place config files in your root folder, that will be used by the various tools. If these files already exist, they will *not* be overwritten.
2) Place the git pre-commit hook in the .git directory. 


### Config files used: 

| File             | Description                                             |
|------------------|---------------------------------------------------------|
| .gitlab-ci.yml   | contains the basic config defining what needs to be run |
| phpcs.xml        | contains the config for phpcs                           |
| phpunit.xml.dist | contains the config for phpunit                         |

## Remarks

Codesniffer by default will run on the PSR1/2 standards, with one exception, the line length rule. This rule has been modified to throw a warning on exceeding 160 characters and throw an error on exceeding 200 characters. I changed that rule because I found the base PSR rule on line length to be quite restrictive.

Gitlab CI also won't fail on warnings for Codesniffer, only on errors. 